#include <tp5.h>
#include <QApplication>
#include <time.h>
#include <iostream>

MainWindow* w = nullptr;


std::vector<string> TP5::names(
{
    "Yolo", "Anastasiya", "Clement", "Sirine", "Julien", "Sacha", "Leo", "Margot",
    "JoLeClodo", "Anais", "Jolan", "Marie", "Cindy", "Flavien", "Tanguy", "Audrey",
    "Mr.PeanutButter", "Bojack", "Mugiwara", "Sully", "Solem",
    "Leo", "Nils", "Vincent", "Paul", "Zoe", "Julien", "Matteo",
    "Fanny", "Jeanne", "Elo"
});


int HashTable::hash(std::string element)
{
    // use this->size() to get HashTable size
    int sum = 0;
    for (char c : element) {
        sum += (int)c;
    }
    return sum % this->size();
}

void HashTable::insert(std::string element)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i = hash(element);
    (*this)[i] = element;
}

/**
 * @brief buildHashTable: fill the HashTable with given names
 * @param table table to fiil
 * @param names array of names to insert
 * @param namesCount size of names array
 */
void buildHashTable(HashTable& table, std::string* names, int namesCount)
{
    for (int i = 0; i < namesCount; i++) {
        table.insert(names[i]);
    }
}

bool HashTable::contains(std::string element)
{
    // Note: Do not use iteration (for, while, ...)
    int index = hash(element);
    return (*this)[index] == element;
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 10;
	w = new HashWindow();
	w->show();

    HashTable hashTable(30); // Create a hash table of size 30
    buildHashTable(hashTable, &TP5::names[0], TP5::names.size());

    // Example usage:
    std::string searchName = "Anais";
    bool isContained = hashTable.contains(searchName);
    std::cout << "Is '" << searchName.c_str() << "' in the hash table? " << (isContained ? "Yes" : "No");

	return a.exec();
}
