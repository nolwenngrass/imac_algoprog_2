#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    if (2*nodeIndex+1 < int(size())){
        return this->get(2*nodeIndex+1);
    }
    return 0;
}

int Heap::rightChild(int nodeIndex)
{
    if (2*nodeIndex+2 < int(size())){
        return this->get(2*nodeIndex+2);
    }
    return 0;
}

void Heap::insertHeapNode(int heapSize, int value)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i = heapSize;
    (*this)[i] = value;
    while (i > 0 && (*this)[i] > (*this)[(i-1)/2]){
        swap((*this)[i], (*this)[(i-1)/2]);
        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i_max = nodeIndex;
    int largest = i_max;
    if ((*this)[i_max] < (*this)[2*i_max+1]){
        largest = 2*i_max+1;
    }
    if (2*i_max+2 < heapSize && (*this)[largest] < (*this)[2*i_max+2]){
        largest = 2*i_max+2;
    }
    if (largest != i_max){
        swap((*this)[i_max], (*this)[largest]);
        heapify(heapSize, largest);
    }
}

void Heap::buildHeap(Array& numbers)
{
    int heapSize = 0;
    insertHeapNode(heapSize, numbers.get(0));
    for (int i = 1; i < int(numbers.size()); i++){
        insertHeapNode(heapSize, numbers.get(i));
        heapify(heapSize + 1, i);
        heapSize++;
    }
}

void Heap::heapSort()
{
    int n = int((*this).size());
    for (int i = n-1; i > 0; i--){
        swap((*this)[0], (*this)[i]);
        heapify(i, 0);
    }
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
    w->show();

    return a.exec();
}
