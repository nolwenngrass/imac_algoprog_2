#include "tp6.h"
#include <QApplication>
#include <time.h>
#include <queue>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int** adjacencies, int nodeCount)
{
    /**
     * Make a graph from a matrix
     * first create all nodes, add them to the graph, and then connect them
     * this->appendNewNode
     * this->nodes[i]->appendNewEdge
     */
    for (int i = 0; i < nodeCount; i++) {
        appendNewNode(new GraphNode(i)); // Create a new node with value equal to its index
    }

    for (int i = 0; i < nodeCount; i++) {
        for (int j = 0; j < nodeCount; j++) {
            if (adjacencies[i][j] == 1) {
                nodes[i]->appendNewEdge(nodes[j]);
            }
        }
    }
}

void Graph::deepTravel(GraphNode* first, GraphNode* nodes[], int& nodesSize, bool visited[])
{
    /**
     * Fill nodes array by traveling graph starting from first and using recursion
     */
    if (!visited[first->value]) {
        visited[first->value] = true;
        nodes[nodesSize++] = first;

        Edge* edge = first->edges;
        while (edge != nullptr) {
            deepTravel(edge->destination, nodes, nodesSize, visited);
            edge = edge->next;
        }
    }
}

void Graph::wideTravel(GraphNode* first, GraphNode* nodes[], int& nodesSize, bool visited[])
{
    /**
     * Fill nodes array by traveling graph starting from first and using a queue
     * nodeQueue.push(a_node)
     * nodeQueue.front() -> first node of the queue
     * nodeQueue.pop() -> remove the first node of the queue
     * nodeQueue.size() -> size of the queue
     */
    std::queue<GraphNode*> nodeQueue;
    nodeQueue.push(first);
    visited[first->value] = true;

    while (!nodeQueue.empty()) {
        GraphNode* current = nodeQueue.front();
        nodeQueue.pop();

        nodes[nodesSize++] = current;

        Edge* edge = current->edges;
        while (edge != nullptr) {
            if (!visited[edge->destination->value]) {
                visited[edge->destination->value] = true;
                nodeQueue.push(edge->destination);
            }
            edge = edge->next;
        }
    }
}

bool Graph::detectCycle(GraphNode* first, bool visited[])
{
    /**
     * Detect if there is a cycle when starting from first
     * (the first may not be in the cycle)
     * Think about what happens when you get an already visited node
     */
    if (visited[first->value])
        return true;

    visited[first->value] = true;

    Edge* edge = first->edges;
    while (edge != nullptr) {
        if (detectCycle(edge->destination, visited))
            return true;
        edge = edge->next;
    }

    visited[first->value] = false;
    return false;
}

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 150;
    w = new GraphWindow();
    w->show();

    return a.exec();
}
