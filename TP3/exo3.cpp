#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

Node* createNode(int value);

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->value = value;
        this->left = nullptr;
        this->right = nullptr;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        SearchTreeNode* newNode = createNode(value);
        newNode->value = value;

        if (value < this->value) {
            if (left == nullptr)
                left = newNode;
            else
                left->insertNumber(value);
        } else {
            if (right == nullptr)
                right = newNode;
            else
                right->insertNumber(value);
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if (left == nullptr && right == nullptr)
                return 1;
            else if (left == nullptr)
                return right->height() + 1;
            else if (right == nullptr)
                return left->height() + 1;
            else
                return std::max(left->height(), right->height()) + 1;

        return 1;
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        uint count = 1;
        if (left != nullptr)
            count += left->nodesCount();
        if (right != nullptr)
            count += right->nodesCount();
        return count;
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        return (left == nullptr && right == nullptr);
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if (isLeaf()) {
            leaves[leavesCount++] = this;
        } else {
            if (left != nullptr)
                left->allLeaves(leaves, leavesCount);
            if (right != nullptr)
                right->allLeaves(leaves, leavesCount);
        }
    }

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travels
        if (left != nullptr)
            left->inorderTravel(nodes, nodesCount);
        nodes[nodesCount++] = this;
        if (right != nullptr)
            right->inorderTravel(nodes, nodesCount);
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount++] = this;
        if (left != nullptr)
            left->preorderTravel(nodes, nodesCount);
        if (right != nullptr)
            right->preorderTravel(nodes, nodesCount);
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if (left != nullptr)
            left->postorderTravel(nodes, nodesCount);
        if (right != nullptr)
            right->postorderTravel(nodes, nodesCount);
        nodes[nodesCount++] = this;
	}

	Node* find(int value) {
        // find the node containing value
        if (value == this->value)
            return this;
        else if (value < this->value && left != nullptr)
            return left->find(value);
        else if (value > this->value && right != nullptr)
            return right->find(value);
        else
            return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
