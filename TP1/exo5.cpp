#include <iostream>

using namespace std;

typedef struct vec2{
    int x;
    int y;
} vec2;

char mandelbrot(vec2 z, vec2 point, int n){
    char rep;
    vec2 f, zCarre;
    int tmp;
    if (z.x*z.x + z.y*z.y > 2*2){
        rep = 'faux';
        return rep;
    }
    if (z.x*z.x + z.y*z.y <= 2*2){ 
        tmp = z.x*z.x - z.y*z.y + point.x;
        z.y = 2*z.x*z.y + point.y;
        z.x = tmp;
        rep = 'vrai';
        return rep;
    }
    if (n == 1){
        return rep;
    }
    mandelbrot(z, point, n-1);
}