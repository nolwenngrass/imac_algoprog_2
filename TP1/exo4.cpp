#include <iostream>

using namespace std;

int allEvens(int evens[], int array[], int evenSize, int arraySize){
    if (array[arraySize]%2 == 0){
        evens[evenSize] = array[arraySize];
        evenSize ++;
    }
    if (arraySize <= 0){
        return evenSize;
    }
    allEvens(evens, array, evenSize, arraySize-1);
}

int main()
{
    int evenSize = 0, arraySize = 0, evens[evenSize], array[arraySize];
    char rep='o';
    while(rep == 'o'){
        cout << "entrez la valeur qui se trouve a la position " << arraySize << endl;
        cin >> array[arraySize];
        cout << "ajouter une valeur ? (o/n)" << endl;
        cin >> rep;
        arraySize ++;
    }
    cout << "il y a " << allEvens(evens, array, evenSize, arraySize)-1 << " nombre(s) pair(s)" << endl;
    return 0;
}