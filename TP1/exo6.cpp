#include <iostream>
#include <stack>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    int size;
    // your code
};

struct DynaTableau{
    int* donnees;
    int size;
    int capacite;
    // your code
};


void initialise(Liste* liste)
{
    int rep;
    cout << "entrez la premiere valeur de la liste" << endl;
    cin >> rep;
    (*liste).premier->donnee = rep;
    (*liste).premier->suivant = nullptr;
    (*liste).size = 1;
}

bool est_vide(const Liste* liste)
{
    if ((*liste).size == 0){
        return true;
    }
    else{
        return false;
    }
}

void ajoute(Liste* liste, int valeur)
{
    for (int i = 0; i<(*liste).size; i++){
      (*liste).premier = (*liste).premier->suivant;
    }
    (*liste).premier->donnee = valeur;
    cout << "AH" << endl;
}

void affiche(const Liste* liste)
{
    cout << liste << endl;
}

int recupere(const Liste* liste, int n)
{
    Liste newListe = *liste;
    for (int i = 1; i<n; i++){
      newListe.premier = newListe.premier->suivant;
    }
    return newListe.premier->donnee;
    return 0;
}

int cherche(const Liste* liste, int valeur)
{
    int indice = 0;
    Liste listeTemp = *liste;
    while (indice<listeTemp.size && listeTemp.premier->donnee != valeur){
        while (listeTemp.premier->suivant != nullptr){
            listeTemp.premier = listeTemp.premier->suivant;
        }
        indice++;
    }
    if (listeTemp.premier == nullptr){
        return -1;
    }
    return indice;
}

void stocke(Liste* liste, int n, int valeur)
{
    for (int i = 0; i<n; i++){
      (*liste).premier = (*liste).premier->suivant;
    }
    (*liste).premier->donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    (*tableau).donnees[(*tableau).size] = valeur;
    (*tableau).size ++;
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau = new DynaTableau[capacite];
    (*tableau).capacite = capacite;
    (*tableau).size = 0;
}

bool est_vide(const DynaTableau* liste)
{
    if ((*liste).size == 0){
        return true;
    }
    else{
        return false;
    }
}

void affiche(const DynaTableau* tableau)
{
    cout << tableau << endl;
}

int recupere(const DynaTableau* tableau, int n)
{
    return (*tableau).donnees[n];
    return 0;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int res = -1;
    for (int i=0; i<(*tableau).size;i++){
      if ((*tableau).donnees[i] == valeur){
        res = i;
      }
    }
    if (res != -1) {
      return res;
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    (*tableau).donnees[n] = valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ;
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    Liste newListe;
    int val = (*liste).premier->donnee;
    for (int i = 1; i<(*liste).size; i++){
      newListe.premier = (*liste).premier;
      (*liste).premier = (*liste).premier->suivant;
    }
    *liste = newListe;
    return val;
    return 0;
}

//void pousse_pile(DynaTableauListe* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud *nouveau;
    nouveau->donnee = valeur;
    nouveau->suivant = liste->premier;
    liste->premier = nouveau;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    Noeud *elementDepile = liste->premier;

    if (liste != nullptr && liste->premier != nullptr)
    {
        liste->premier = elementDepile->suivant;
        free(elementDepile);
    }
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    /***if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }***/

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
