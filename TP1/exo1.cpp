#include <iostream>

using namespace std;

int power(long value, long n){
    if(n==0){
        return 1;
    }
    else if(n==1){
        return value;
    }
    return((power(value, n-1))*value);
}

int main()
{
    long valeur, puissance;
    cout << "entrez un nombre que vous souhaitez elever" << endl;
    cin >> valeur;
    cout << "entrez maintenant la puissance a laquelle vous voulez l'elever" << endl;
    cin >> puissance;
    cout << "le resultat est : " << power(valeur, puissance) << endl;
    return 0;
}