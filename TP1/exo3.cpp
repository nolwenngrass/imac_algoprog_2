#include <iostream>

using namespace std;
int longueurArray = 10;

int search(int value, int array[], int size){
    if (array[size] == value){
    return size;
    }
    search(value, array, size-1);
}

int main()
{
    int valeur, temp, tab[longueurArray], i=1;
    char rep='o';
    cout << "entrez la valeur a chercher" << endl;
    cin >> valeur;
    while(rep == 'o'){
        cout << "entrez la valeur qui se trouve a la position " << i << endl;
        cin >> tab[i];
        cout << "ajouter une valeur ? (o/n)" << endl;
        cin >> rep;
        i++;
    }
    cout << valeur << " est a la position : " << search(valeur,tab,longueurArray) << endl;
    return 0;
}