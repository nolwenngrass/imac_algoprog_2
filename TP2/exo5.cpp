#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
    // stop statement = condition + return (return stop the function even if it does not return anything)
    if (origin.size() < 2){
        return;
    }

	// initialisation
	Array& first = w->newArray(origin.size()/2);
    Array& second = w->newArray(origin.size()-first.size());

	// split
    for (int i=0; i < int(origin.size()); i++){
        if (i < int(origin.size())/2){
            first[i] = origin[i];
        }
        else{
            second[i-int(origin.size())/2] = origin[i];
        }
    }

	// recursiv splitAndMerge of lowerArray and greaterArray
    splitAndMerge(first);
    splitAndMerge(second);

	// merge
    merge(first,second,origin);

    /***t ( tableau de nombre aléatoire
    t1 ( première moitié du t
    t2 ( deuxième moitié t
    diviser et trier t1 et t2
    Pour chaque indice i de t faire
    t[i] ( min(t1, t2)
    fin Pour***/

    /***def fusionner(L1,L2):
        long1,long2,ind1,ind2 = len(L1),len(L2),0,0
        res = [0]*(long1+long2)
        while ind1<long1 and ind2<long2:
            if L1[ind1]<L2[ind2]:
                res[ind1+ind2] = L1[ind1]
                ind1 += 1
            else:
                res[ind1+ind2] = L2[ind2]
                ind2 += 1
        if ind1 == long1:
            res[ind1+ind2:]=L2[ind2:]
        else:
            res[ind1+ind2:] = L1[ind1:]
        return res

    def triFusion(L):
        if len(L)<2:
            return L
        else:
            L1 = L[:len(L)//2]
            L2 = L[len(L)//2:]
            return fusionner(triFusion(L1),triFusion(L2))***/
}

void merge(Array& first, Array& second, Array& result)
{
    int indF = 0, indS = 0;

    while (indF<int(first.size()) && indS<int(second.size())){
        if (first[indF]<second[indS]){
            result[indF+indS] = first[indF];
            indF ++;
        }
        else{
            result[indF+indS] = second[indS];
            indS ++;
        }
    }
    if (indF == indS){
        for (int i=0; i<int(second.size()); i++){
            result[indF+indS+i] = second[indS+i];
        }
    }
    else{
        for (int j=0; j<int(first.size()); j++){
            result[indF+indS+j] = first[indF+j];
        }
    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
