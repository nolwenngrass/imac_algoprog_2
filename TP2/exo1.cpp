#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
	// selectionSort
    int min, indMin;
    for (int j=0; j<int(toSort.size()); j++){
        min = toSort[j];
        indMin = j;
        for (int i=j; i<int(toSort.size()); i++){
            if (toSort[i] < min){
                min = toSort[i];
                indMin = i;
            }
        }
        toSort.swap(indMin,j);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
